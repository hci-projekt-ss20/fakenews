window.addEventListener("load", () => {
    buildTaskContainer();
    buildSliderElements();
    showDivs(3);
    buildRdmArticle();
})

function showContent() {
    document.getElementById("task_container").style.display = "none";
    startTime = (new Date()).getTime();
}

function showHome() {
    document.getElementById("tag_overview").style.display = "none";
    document.getElementById("full_article").style.display = "none";
    document.getElementById("article_preview").style.display = "inline";
    document.getElementById("slider").style.display = "block";
}

//Build container with concrete task
function buildTaskContainer() {
    console.log(articleToBeFound.title);
    if(articleToBeFound.tag != undefined){
        taskDescription = "Find the article [" + articleToBeFound.title + "] within the category " + articleToBeFound.tag + ".";
    } else {
        taskDescription = "Find the article [" + articleToBeFound.title + "] on main page.";
    }
    let task_container = document.getElementById("task_container");
    let paragraph = document.createElement("p");
    paragraph.textContent = taskDescription;

    let button = document.createElement("button");
    button.setAttribute("id", "start_test");
    button.setAttribute("onclick", "showContent()");
    button.innerHTML = "Start Test";
    task_container.appendChild(paragraph);
    task_container.appendChild(button);
}
//Build Article HTML for article preview
function buildRdmArticle() {
    let article_location = document.getElementById("article_preview");
    for (let i = 0; i < 8; i++) {
        let article = articlesPreview[i];

        let aTag = document.createElement("a");
        aTag.setAttribute("href", "javascript:generateArticle('"+article.title+"','"+article.image+"')");
        if (article.title == articleToBeFound.title) {
            aTag.setAttribute("onclick", "javascript:stopTest()");
        }

        let paragraph = document.createElement("p");
        paragraph.textContent = lorem_ipsum_preview;

        let image = document.createElement("img");
        image.setAttribute("src", article.image);
        image.setAttribute("class", "left");
        image.setAttribute("width", "640");
        image.setAttribute("height", "360");

        let headline = document.createElement("h2");
        headline.innerHTML = article.title;

        aTag.appendChild(headline);

        let inner_div = document.createElement("div");
        inner_div.appendChild(aTag);
        inner_div.appendChild(paragraph);

        let outer_div = document.createElement("div");
        outer_div.setAttribute("class", "article");

        outer_div.appendChild(image);
        outer_div.appendChild(inner_div);
        article_location.appendChild(outer_div);
    }
}

//Show all articles sharing a tag
function generateTagView(tag) {

    let article_location = document.getElementById("tag_overview");
    article_location.innerHTML = '';
    document.getElementById("slider").style.display = "none";
    document.getElementById("article_preview").style.display = "none";
    document.getElementById("full_article").style.display = "none";
    article_location.style.display = "inline";

    articlesTagviews.forEach(element => {
        if (element.tag == tag) {

            let aTag = document.createElement("a");
            aTag.setAttribute("href", "javascript:generateArticle('"+element.title+"','"+element.image+"')");
            if (element.title == articleToBeFound.title) {
                aTag.setAttribute("onclick", "javascript:stopTest()");
            }

            let paragraph = document.createElement("p");
            paragraph.textContent = lorem_ipsum_preview;

            let image = document.createElement("img");
            image.setAttribute("src", element.image);
            image.setAttribute("class", "left");
            image.setAttribute("width", "640");
            image.setAttribute("height", "360");

            let headline = document.createElement("h2");
            headline.innerHTML = element.title;

            aTag.appendChild(headline);

            let inner_div = document.createElement("div");
            inner_div.appendChild(aTag);
            inner_div.appendChild(paragraph);

            let outer_div = document.createElement("div");
            outer_div.setAttribute("class", "article");

            outer_div.appendChild(image);
            outer_div.appendChild(inner_div);
            article_location.appendChild(outer_div);
        }

    }); 
}

//script functions for slider buildSliderElements(), plusDivs(), showDivs()
var slideIndex = 3;

function buildSliderElements() {
    let article_location = document.getElementById("slider");
    let aTagPrev = document.createElement("a");
    aTagPrev.setAttribute("class", "prev");
    aTagPrev.setAttribute("onclick", "plusDivs(-3)");
    aTagPrev.innerHTML = "&#10094;";
    article_location.appendChild(aTagPrev);
    for (let i = 0; i < 6; i++) {
        let articleSlider = articlesSlider[i];

        let aTag = document.createElement("a");
        aTag.setAttribute("href", "javascript:generateArticle('"+articleSlider.title+"','"+articleSlider.image+"')");
        if (articleSlider.title == articleToBeFound.title) {
            aTag.setAttribute("onclick", "javascript:stopTest()");
        }

        let headline = document.createElement("h2");
        headline.innerHTML = articleSlider.title;

        let image = document.createElement("img");
        image.setAttribute("src", articleSlider.image);
        image.setAttribute("width", "400");
        image.setAttribute("height", "200");

        let div = document.createElement("div");
        div.setAttribute("class", "slider_element");

        aTag.appendChild(headline);
        div.appendChild(image);
        div.appendChild(aTag);
        article_location.appendChild(div);
    }
    let aTagNext = document.createElement("a");
    aTagNext.setAttribute("class", "next");
    aTagNext.setAttribute("onclick", "plusDivs(3)");
    aTagNext.innerHTML = "&#10095;";
    article_location.appendChild(aTagNext);

}
//calculation for divs to show inside slider plusDivs(), showDivs()
function plusDivs(n) {
    slideIndex += n;
    if (slideIndex > 6) {
        slideIndex = 3
    }
    if (slideIndex <= 0) {
        slideIndex = 6;
    }
    showDivs(slideIndex);
}

function showDivs(n) {
    var i;
    var x = document.getElementsByClassName("slider_element");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    if (slideIndex == 3) {
        x[0].style.display = "inline-block";
        x[1].style.display = "inline-block";
        x[2].style.display = "inline-block";
    }
    if (slideIndex == 6) {
        x[3].style.display = "inline-block";
        x[4].style.display = "inline-block";
        x[5].style.display = "inline-block";
    }
}  

//Generate HTML to vhow article
function generateArticle(title, image_location) {

    let article = document.getElementById("full_article");
    article.innerHTML = '';
    document.getElementById("slider").style.display = "none";
    document.getElementById("article_preview").style.display = "none";
    document.getElementById("tag_overview").style.display = "none";
    article.style.display = "inline";
    
    let headline = document.createElement("h1");
    headline.innerHTML = title;

    let image = document.createElement("img");
    image.setAttribute("src", image_location);

    let paragraph = document.createElement("p");
    paragraph.textContent = lorem_ipsum;

    article.appendChild(image);
    article.appendChild(headline);
    article.appendChild(paragraph);
}

function stopTest() {
    let time = ((new Date()).getTime() - startTime)/1000;
    alert("It took you " + time + " seconds to find the article");
}