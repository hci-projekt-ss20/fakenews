
let lorem_ipsum_preview = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."
let lorem_ipsum = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.".repeat(10);
let taskDescription;
let taskPreview = getRandomInt(8);
let taskSlider = getRandomInt(6);
let taskTagged = getRandomInt(24);
let articleToBeFound;
let startTime;

//Article constructor
function Article(title, image, tag) {
    this.title = title;
    this.image = image;
    this.tag = tag;
}


function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}
//Shuffle function array
function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

//Shuffle all articles for random picks in preview
let articlesTagviews = shuffle([
    new Article("North Korea threatens South Korea", "images/nskorea.png", "Politics"),
    new Article("The United States left the NATO", "images/nato.png", "Politics"),
    new Article("Protests in the UK demand a reentrie into the European Union", "images/brexit.png", "Politics"),
    new Article("Wirecard share falls to record low", "images/wirecard.png", "Stock Market"),
    new Article("Lufthansa major shareholder agrees to state aid", "images/lufthansa.png", "Stock Market"),
    new Article("Zalando reports bankruptcy", "images/zalando.png", "Stock Market"),
    new Article("Australian fire hold on since 4 months", "images/australia.png", "Abroad"),
    new Article("California legalized weed", "images/weed.png", "Abroad"),
    new Article("Texas strives for changes in gun laws", "images/texas.png", "Abroad"),
    new Article("First record summer since 2007", "images/summer.png", "Weather"),
    new Article("Thunderstorms expected all over Germany", "images/thunder.png", "Weather"),
    new Article("-10 degrees Celcius forecasts for the next 2 weeks", "images/degree.png", "Weather"),
    new Article("Cologne Cathedral is to be restored", "images/cathedral.png", "Inland"),
    new Article("Rental prices exploded in Munich", "images/munich.png", "Inland"),
    new Article("Museumsuferfest in Frankfurt expects 100,000 visitors", "images/frankfurt.png", "Inland"),
    new Article("Complaint against gambling providers", "images/gambling.png", "Investigative"),
    new Article("Softcraft employs Russia lobbyists", "images/russia.png", "Investigative"),
    new Article("LKA warned Landesbank of loopholes", "images/lka.png", "Investigative"),
    new Article("FC Bayern Munich is the Champions League Winner 2020", "images/fcbayern.png", "Sports"),
    new Article("Neymar transfer to Barcelona failed", "images/neymar.png", "Sports"),
    new Article("Messi once again wins Ballon D'or", "images/messi.png", "Sports"),
    new Article("Gross domestic product falls 5%", "images/grossproduct.png", "Economy"),
    new Article("Volkswagen records losses about 1 billion", "images/vw.png", "Economy"),
    new Article("Wirecard and Deutsche Bank planned fusion", "images/deutschebank.png", "Economy"),
]);

//Shuffle all articles for random picks in slider
let articlesSlider = shuffle([
    new Article("Mastercard and Visa reportedly reconsidering their relationship with Wirecard following accounting scandal", "images/visa.png"),
    new Article("Apple, Google, Amazon and Facebook bet on gaming", "images/google.png"),
    new Article("Number of virus infections tops 10 million world wide", "images/covid2.png"),
    new Article("Maguire sends Manchester United into FA cup semis", "images/maguire.png"),
    new Article("Conor McGregor back at the top of UFC", "images/mcgregor.png"),
    new Article("NATO commander demands pledges for Corona contingency plan", "images/nato.png"),
    new Article("Bavaria plans corona tests for everyone", "images/bavaria.png"),
    new Article("Bremen saves itself in the relegation", "images/bremen.png"),
    new Article("Facebook reacts to advertising boycott", "images/fb.png"),
    new Article("Corona warning app works despite error message", "images/covid2.png"),
    new Article("127 routers sometimes have significant security problems", "images/router.png"),
    new Article("Podcast with Christian Mustermann receives two awards", "images/pod.png"),
    new Article("Scientists call for more transparency from Facebook and Co.", "images/fb.png"),
    new Article("IT experts call for rethinking password rules", "images/it.png"),
    new Article("Holidaymakers are increasingly renting mobile homes", "images/holiday.png"),
    new Article("Söder warns of an early second wave - also because of the summer tourists", "images/söder.png"),
]);

//Shuffle all articles for random picks in preview
let articlesPreview = shuffle([
    new Article("Brandenburg gate is to be restored", "images/brandenburg.png"),
    new Article("Microsoft manager Jim Schneider resigns", "images/microsoft.png"),
    new Article("Ford one of the most popular automotive manufacturer thanks to Peter Lawrence", "images/ford.png"),
    new Article("Corona outbreak at Tönnies", "images/tönnies.png"),
    new Article("Englands prime minister resigns", "images/england.png"),
    new Article("Trump campaign had social distancing stickers removed before Tulsa rally", "images/trump.png"),
    new Article("Liverpool star Alexander-Arnold bids to build new dynasty", "images/alexander.png"),
    new Article("Architects make a new kind oc cement", "images/cement.png"),
    new Article("Donald Trump does not reach majority", "images/trump.png"),
    new Article("PSG gets high fines because of financial fairplay violations", "images/psg.png"),
    new Article("Portugal wins world championship", "images/portugal.png"),
    new Article("Automotive industry is losing confidence", "images/automotive.png"),
    new Article("Gold price rises unexpectedly", "images/gold.png"),
    new Article("Share prices stable again after COVID-19", "images/share.png"),
    new Article("Deutsche Bahn manager Thomas Hoffmann resigns", "images/db.png"),
    new Article("John Becker accused of insider trades", "images/trades.png"),
    new Article("Jürgen Klopp wins Premier League with Liverpool", "images/klopp.png"),
    new Article("Cure against COVID-19 expected in 2021", "images/covid.png"),
    new Article("DAX reaches new record low", "images/dax.png"),
    new Article("Ajax Amsterdam secures deal with Adidas", "images/ajax.png"),
    new Article("Otto reports bankruptcy", "images/otto.png"),
    new Article("Tolisso transfer to Manchester United failed", "images/manu.png"),
    new Article("Beyond meat records record earnings", "images/beyond.png"),
    new Article("Apple more popular than Samsung in surveys", "images/apple.png"),
]);

let possibleTasks = shuffle([
    articlesTagviews[taskTagged],
    articlesPreview[taskPreview],
    articlesSlider[taskSlider]
]);
 articleToBeFound = possibleTasks[0];